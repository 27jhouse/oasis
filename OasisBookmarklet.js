/* I add random characters at the end of the ID so that it does not clash with any other ID's on the page */

const ID =
  "--picture-in-picture-toolbar-8743fsfjkl9274g9832fkjdslfjksl7498247389"; /* Helper functions */
const get = (selector, el = document) => el.querySelector(selector);
const getAll = (selector, el = document) =>
  Array.from(el.querySelectorAll(selector));
/* Remove our toolbar if it exists */ let _toolbar = get(`#${ID}`);
if (_toolbar) {
  _toolbar.parent.removeChild(_toolbar);
}
/* create the toolbar */ const toolbar = document.createElement(
  "div"
); /* Set the toolbar's ID, so we can remove it later */
toolbar.id = ID; /* Use shadow-dom as outlined here: https://developers.google.com/web/fundamentals/web-components/shadowdom */
/* Inside shadow dom we don't have to worry about conflicting styles. */ const shadowroot = toolbar.attachShadow(
  { mode: "open" }
);
shadowroot.innerHTML = `<div id="container"> 
<style>
body {
  font-family: 'Lato', sans-serif;
}

.overlay {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0, 0.5);
  overflow-x: hidden;
  transition: 0.5s;
}

.overlay-content {
  position: relative;
  top: 25%;
  width: 100%;
  text-align: center;
  margin-top: 30px;
}

.overlay a {
  padding: 8px;
  text-decoration: none;
  font-size: 36px;
  color: black;
  display: block;
  transition: 0.3s;
}

.overlay a:hover, .overlay a:focus {
  color: black;
}

.overlay .closebtn {
  position: absolute;
  top: 20px;
  right: 45px;
  font-size: 60px;
}

@media screen and (max-height: 450px) {
  .overlay a {font-size: 20px}
  .overlay .closebtn {
  font-size: 40px;
  top: 15px;
  right: 35px;
  }
}

.app {
    text-align: center;
background-color: white;
font-size: 15px;
  min-height: 45px;
  min-width: 45px;
  margin: 10px;

  padding: 5px;
  border-radius: 100px;
}


#openNav {
    position: fixed;
    bottom: 0;
    left: 0;
    padding: 10px;
}
</style>


<div id="Player" class="overlay">
  <a href="javascript:void(0)" background-color="black" class="closebtn" id="closePlayer" onclick="closeNav()">&times;</a>
  <div class="overlay-content">
  URL:<input id="URL" type="text"></input><button id="GO">GO</button>
   <iframe id="frame" src="" width="100%" height="600px">

   </iframe>
  </div>
</div>
 

<div id="myNav" class="overlay">
  <a href="javascript:void(0)" class="closebtn" id="closeNav" onclick="closeNav()">&times;</a>
  <div class="overlay-content">
<button class="app" id="openPlayer">
Webplayer
</button>
<button class="app" id="GBA">
GBA-jack
</button>
  </div>
</div>
 

<button id="openNav">
+
</button>

  <button aria-label="close" id="close">    x  </button>
  </div>  
  
  `;

shadowroot.getElementById("close").style.display = "none";

get("#close", shadowroot).addEventListener("click", () => {
  const toolbar = get(`#${ID}`);
  if (toolbar) {
    toolbar.parentNode.removeChild(toolbar);
  }
});

get("#openNav", shadowroot).addEventListener("click", () => {
 openNav()
});
get("#closeNav", shadowroot).addEventListener("click", () => {
 closeNav()
});
get("#openPlayer", shadowroot).addEventListener("click", () => {
 openPlayer()
 closeNav()
});
get("#closePlayer", shadowroot).addEventListener("click", () => {
 closePlayer()
});

get("#GO", shadowroot).addEventListener("click", () => {
shadowroot.getElementById('frame').src = shadowroot.getElementById('URL').value
});





get("#GBA", shadowroot).addEventListener("click", () => {
Play('https://gba-jack.netlify.app/','frame')
});





function Play(url,frame) {
    shadowroot.getElementById(frame).src = url
    closeNav()
    openPlayer()
}

function openNav() {
  shadowroot.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  shadowroot.getElementById("myNav").style.width = "0%";
}

function openPlayer() {

  shadowroot.getElementById("Player").style.width = "100%";
}

function closePlayer() {
  shadowroot.getElementById("Player").style.width = "0%";
}




document.addEventListener("keyup", function(event) {
    if (event.key === 'Alt') {
      openNav()
    }
});

document.body.appendChild(toolbar);
